/**
 * Created by hanni on 11/7/16.
 */
angular.module('app', []);

angular.module('app').controller('MainController', ['$http',MainController]);

function MainController($http) {
    var self = this;
    self.idPost = '';

    self.posIdText = '';

    self.postAuthor = '';
    self.postTitle = '';
    self.postText = '';

    self.updatedText = [];
    self.updatedTitle = [];

    self.posts = [];
    self.rating = [];

    self.addNewPost = function addNewPost() {
        if (self.postText.length == 0 ) {
            alert('Vstav text');
            return;
        }
         var obj = {
             title: self.postTitle,
             text: self.postText,
             author: self.postAuthor
         };

        $http.post('/posts', obj)
            .then(function success(res) {

                self.postAuthor = '';
                self.postTitle = '';
                self.postText = '';
                self.getPosts();
            }, function error(err) {
                console.log(err);
        });
    };

    self.getPostById = function getPostById(){
        if (self.idPost.length > 0) {

            $http.get('/posts/' + self.idPost)
                .then(function success(res) {
                    console.log(res.data);
                    self.posts = [];
                    self.posts.push(res.data);
                    self.updatedText = [''];
                    self.updatedTitle = [''];

                }, function error(err) {
                console.log('err');
            });

        } else {
            alert('Input id');
        }
    };

    self.updatePost = function updatePost(index, arrIndex) {

            if (self.updatedText[arrIndex].length < 1 && self.updatedTitle[arrIndex].length < 1) {
                alert('Input value');
                return null;
            }
                $http.put('/posts/' + index, {
                    text: self.updatedText[arrIndex],
                    title: self.updatedTitle[arrIndex]
                })
                    .then(function success(res) {
                        // self.getPosts();

                        if(!self.updatedText[arrIndex]) {
                            self.posts[arrIndex].title = self.updatedTitle[arrIndex];
                        } else if (!self.updatedTitle[arrIndex]) {
                            self.posts[arrIndex].text = self.updatedText[arrIndex];
                        } else {
                            self.posts[arrIndex].title = self.updatedTitle[arrIndex];
                            self.posts[arrIndex].text = self.updatedText[arrIndex];
                        }

                    }, function error(err) {
                        console.log(err);
                    });

    };

    self.getPosts = function getAllPosts() {
        $http.get('/posts')
            .then(function success(res) {
                console.log(res.data);
                self.posts = res.data;
                self.updatedText = [];
                self.updatedTitle = [];

                self.posts.forEach( function () {
                   self.updatedText.push('');
                   self.updatedTitle.push('');

                });
            }, function error(err) {
                console.log(err);
        });
    };

    self.getPosts();

    self.deletePost = function deletePost(index) {

        $http.delete('/posts/' + index)
            .then( function success(req) {
                console.log(req.statusText);
                self.getPosts();
            }, function error(err) {
               console.log(err);
            });
    };

    self.updateRating = function updateRating(rating, id, index) {

            if (rating){
                self.obj = {
                    rating: 1
                }
            } else {
                self.obj = {
                    rating: -1
                }
            }


            $http.put('/posts/rating/' + id, self.obj)
                .then( function success(req) {
                    self.posts[index].rating += self.obj.rating;
                }, function error(err) {
                    console.log(err);
                });
    }
}