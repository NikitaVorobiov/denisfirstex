/**
 * Created by hanni on 11/7/16.
 */
var express = require('express');
var app = express();
var bodyParser = require('body-parser')

var posts = [];
var rating = [];

var post = {
    index: 3,
    text: 'adasdads',
    rating: 23
};

app.use(express.static(__dirname));
app.use(bodyParser.json());


var entry = require('./controllers/entry');

app.get('/posts', entry.getAllPosts);
app.get('/posts/:id', entry.getPostById);
app.put('/posts/:id', entry.updatePost);
app.delete('/posts/:id', entry.deletePost);
app.put('/posts/rating/:id',entry.updateRating);
app.post('/posts', entry.addNewPost);

var server = app.listen('3000', function () {
   console.log('Server listening on port: ' + server.address().port);
});