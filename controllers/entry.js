/**
 * Created by hanni on 11/8/16.
 */
var Uuid = require('node-uuid');
var fs = require('fs');
var _ = require('underscore');

var dataDir = './data/'

exports.getAllPosts = function getAllPosts(req, res) {

    var posts = [];

    fs.readdir(dataDir, function (err, files) {
        if (err) {
            res.status(401).send('Failed read directory with entries');
        }

        _.each(files, function (nameData) {
            var obj = fs.readFileSync('./data/' + nameData);
            posts.push(JSON.parse(obj.toString()));
        });
        res.send(posts);
    });
};

exports.getPostById = function getPostById(req, res) {
    if (!req.params.id) {
        res.status(409).send('id is null');
        return null;
    }

    fs.readFile(dataDir + req.params.id + '.json', function (err, data) {
        if (err) {
            res.status(404).send('Post ' + req.params.id + ' not found');
            return null;
        }

        res.send(JSON.parse(data.toString()));
    });
};

exports.addNewPost = function addNewPost(req, res) {

    if (!req.body.title) {
        res.status(409).send('Title is null');
        return null;
    }
    if (!req.body.text) {
        res.status(409).send('Text is null');
        return null;
    }
    if (!req.body.author) {
        res.status(409).send('Author is null');
        return null;
    }

    var idPost = 'id_' + Uuid.v4();

    var obj = {
        index: idPost,
        title: req.body.title,
        text: req.body.text,
        author: req.body.author,
        rating: 0
    };

    obj = JSON.stringify(obj);

    fs.writeFile(dataDir + idPost + '.json', obj, function (err) {
        if (err) {
            res.status(401).send('Failed to add entry');
            return null;
        }
    });
    res.send('Success');
};

exports.updatePost = function updatePost(req, res) {
    if (!req.params.id) {
        res.status(409).send('id is null');
        return null;
    }
    if(!req.body.text && !req.body.title) {
        res.status(409).send('Text and Title is null');
        return null;
    }

    fs.readFile(dataDir + req.params.id + '.json', function (err, data) {
        if (err) {
            res.status(404).send('Post ' + req.params.id + ' not found');
            return null;
        }

        console.log(req.body.title );
        console.log(req.body.text );
        if (!req.body.title || req.body.title == 'undefined') {
            var obj = JSON.parse(data);
                obj.text = req.body.text;
        }
        else if (!req.body.text || req.body.text == 'undefined') {
            var obj = JSON.parse(data);
                obj.title =  req.body.title;
        }
        else {
            var obj = JSON.parse(data);
                obj.title = req.body.title;
                obj.text = req.body.text;
        }

        obj = JSON.stringify(obj);

        fs.writeFile(dataDir + req.params.id + '.json', obj, function (err) {
            if (err) {
                res.status(401).send('Can not update');
            }
            res.send('Success');
        });




    });



};

exports.deletePost = function deletePost(req, res) {
    if (!req.params.id) {
        res.status(409).send('id is null');
        return null;
    }

    fs.unlink(dataDir + req.params.id + '.json', function (err) {
        if (err) {
            res.status(401).send('Can not delete');
            return null;
        }

        res.send('Success');
    });

};

exports.updateRating = function updateRating(req, res) {
    if (!req.params.id) {
        res.status(409).send('id is null');
        return null;
    }
    if (!req.body.rating) {
        res.status(409).send('rating is null');
        return null;
    }

    // if (req.body.rating == 1) {
    //     var rating = 1;
    // } else {
    //     var rating = -1;
    // }

    fs.readFile(dataDir + req.params.id + '.json', function (err, data) {
        if (err) {
            res.status(404).send('Post ' + req.params.id + ' not found');
            return null;
        }

            var obj = JSON.parse(data);
            obj.rating += req.body.rating;

        obj = JSON.stringify(obj);

        fs.writeFile(dataDir + req.params.id + '.json', obj, function (err) {
            if (err) {
                res.status(401).send('Can not update');
            }
            res.send('Success');
        });
    });

};


